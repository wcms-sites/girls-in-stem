core = 7.x
api = 2


/**
 * Libraries
 * Alphabetically ordered
 */
 
; CKEditor 4
; http://ckeditor.com/download
libraries[ckeditor][download][type] = "git"
libraries[ckeditor][download][url] = "https://git.uwaterloo.ca/libraries/ckeditor.git"
libraries[ckeditor][download][tag] = "4.5.6"

; CKEditor CodeMirror Plugin
; https://github.com/w8tcha/CKEditor-CodeMirror-Plugin
libraries[codemirror][download][type] = "git"
libraries[codemirror][download][url] = "https://git.uwaterloo.ca/libraries/CKEditor-CodeMirror-Plugin.git"
libraries[codemirror][download][branch] = "master"
libraries[codemirror][download][revision] = "457d266f"
libraries[codemirror][directory_name] = "codemirror"

; CKEditor Text Selection Plugin
; https://github.com/w8tcha/CKEditor-TextSelection-Plugin/
libraries[textselection][download][type] = "git"
libraries[textselection][download][url] = "https://git.uwaterloo.ca/libraries/CKEditor-TextSelection-Plugin.git"
libraries[textselection][download][branch] = "master"
libraries[textselection][download][revision] = "c439c4e0"
libraries[textselection][directory_name] = "textselection"